class WelcomeController < ApplicationController
  def index
    @user = User.new("Reinaldo", "reinaldo122@gmail.com", "reinaldo")
    UserMailer.welcome_email(@user).deliver
  end
end
